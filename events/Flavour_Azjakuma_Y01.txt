namespace = flavor_azjakuma

country_event = {
	id = flavor_azjakuma.1
	title = flavor_azjakuma.1.t
	desc = flavor_azjakuma.1.d
	picture = CITY_VIEW_eventPicture
	
	fire_only_once = yes
	is_triggered_only = yes
	
	trigger = {
		tag = Y01
		NOT = { has_country_flag = oni_human_tolerance_setup }
	}
	
	mean_time_to_happen = {
		days = 1
	}
	
	hidden = yes
	
	option = {		
		name = flavor_azjakuma.1.a
		ai_chance = { factor = 30 }	
		change_variable = {
			which = human_race_tolerance
			value = 50
		}
		set_country_flag = oni_human_tolerance_setup
	}
}

country_event = {
	id =  flavor_azjakuma.2
	title =  flavor_azjakuma.1.t
	desc =  flavor_azjakuma.1.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	fire_only_once = yes
	is_triggered_only = yes
	hidden = yes
	
	trigger = {
		tag = Y01
		always = yes
	}
	
	option = {
		name = flavor_azjakuma.1.a
		ai_chance = { factor = 100 }
		Y01 = {
			add_opinion = { who = Y89 modifier = loyal_servants }
			reverse_add_opinion = { who = Y89 modifier = noble_oni }
		}
	}
}

country_event = {
	id = flavor_azjakuma.3
	title =  flavor_azjakuma.1.t
	desc =  flavor_azjakuma.1.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	fire_only_once = yes
	is_triggered_only = yes
	hidden = yes
	
	trigger = {
		tag = Y01
		always = yes
	}
	
	option = {
		name = flavor_azjakuma.1.a
		ai_chance = { factor = 100 }
		Y01 = {
			add_faction_influence = {
				faction = oni_highest_fire
				influence = 35
			}
			add_faction_influence = {
				faction = oni_silent_mist
				influence = 25
			}
			add_faction_influence = {
				faction = oni_bright_claw
				influence = 30
			}
			add_faction_influence = {
				faction = oni_golden_gates
				influence = 20
			}
		}
	}
}

country_event = {
	id = flavor_azjakuma.4
	title =  flavor_azjakuma.4.t
	desc =  flavor_azjakuma.4.d
	picture = ELECTION_REPUBLICAN_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		has_reform = tagharoghi_reform
		NOT = { has_country_flag = in_oni_heir_selection }
	}
	
	immediate = {
		hidden_effect = {
			set_country_flag = in_oni_heir_selection
		}
	}
	
	option = {
		name = flavor_azjakuma.4.a
		ai_chance = { factor = 100 }
		trigger = {
			has_faction = oni_highest_fire
		}
		add_adm_power = 75
		define_heir = {
			dynasty = "Aromo"
			age = 40
			claim = 90
			max_random_adm = 5
			max_random_dip = 5
			max_random_mil = 5
		}
		add_faction_influence = {
			faction = oni_highest_fire
			influence = 15
		}
		clr_country_flag = in_oni_heir_selection
	}
	
	option = {
		name = flavor_azjakuma.4.b
		ai_chance = { factor = 100 }
		trigger = {
			has_faction = oni_silent_mist
		}
		add_dip_power = 75
		define_heir = {
			dynasty = "Talshimok"
			age = 30
			claim = 90
			max_random_adm = 4
			max_random_dip = 6
			max_random_mil = 4
		}
		add_faction_influence = {
			faction = oni_silent_mist
			influence = 15
		}
		clr_country_flag = in_oni_heir_selection
	}
	
	option = {
		name = flavor_azjakuma.4.c
		ai_chance = { factor = 100 }
		trigger = {
			has_faction = oni_bright_claw
		}
		add_mil_power = 75
		define_heir = {
			dynasty = "Surgoshi"
			age = 20
			claim = 90
			max_random_adm = 4
			max_random_dip = 4
			max_random_mil = 6
		}
		add_faction_influence = {
			faction = oni_bright_claw
			influence = 15
		}
		clr_country_flag = in_oni_heir_selection
	}
	
	option = {
		name = flavor_azjakuma.4.e
		ai_chance = { factor = 100 }
		trigger = {
			has_faction = oni_golden_gates
		}
		add_years_of_income = 1.25
		define_heir = {
			dynasty = "Moguruld"
			age = 30
			claim = 90
			max_random_adm = 6
			max_random_dip = 4
			max_random_mil = 4
		}
		add_faction_influence = {
			faction = oni_golden_gates
			influence = 15
		}
		clr_country_flag = in_oni_heir_selection
	}
	
	option = {
		name = flavor_azjakuma.4.f
		ai_chance = { factor = 100 }
		trigger = {
			has_faction = oni_cursed_souls
		}
		add_adm_power = 25
		add_dip_power = 25
		add_mil_power = 25
		define_heir = {
			dynasty = "Jishuro"
			age = 30
			claim = 90
			max_random_adm = 6
			max_random_dip = 6
			max_random_mil = 6
		}
		add_faction_influence = {
			faction = oni_cursed_souls
			influence = 15
		}
		clr_country_flag = in_oni_heir_selection
	}
}

country_event = {
	id = flavor_azjakuma.5
	title =  flavor_azjakuma.5.t
	desc =  flavor_azjakuma.5.d
	picture = COUNTRY_COLLAPSE_eventPicture
	
	trigger = {
		tag = Y01
		has_faction = oni_highest_fire
		NOT = { owns_or_subject_of = 5430 }
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	option = {
		name = flavor_azjakuma.5.a
		ai_chance = { factor = 100 }
		add_stability = -1
		add_devotion = -10
		remove_faction = oni_highest_fire
	}
}

country_event = {
	id = flavor_azjakuma.6
	title =  flavor_azjakuma.6.t
	desc =  flavor_azjakuma.6.d
	picture = COUNTRY_COLLAPSE_eventPicture
	
	trigger = {
		tag = Y01
		has_faction = oni_silent_mist
		NOT = { owns_or_subject_of = 4831 }
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	option = {
		name = flavor_azjakuma.6.a
		ai_chance = { factor = 100 }
		add_stability = -1
		add_devotion = -10
		remove_faction = oni_silent_mist
	}
}

country_event = {
	id = flavor_azjakuma.7
	title =  flavor_azjakuma.7.t
	desc =  flavor_azjakuma.7.d
	picture = COUNTRY_COLLAPSE_eventPicture
	
	trigger = {
		tag = Y01
		NOT = { owns_or_subject_of = 4829 }
		has_faction = oni_bright_claw
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	option = {
		name = flavor_azjakuma.7.a
		ai_chance = { factor = 100 }
		add_stability = -1
		add_devotion = -10
		remove_faction = oni_bright_claw
	}
}

country_event = {
	id = flavor_azjakuma.8
	title =  flavor_azjakuma.8.t
	desc =  flavor_azjakuma.8.d
	picture = COUNTRY_COLLAPSE_eventPicture
	
	trigger = {
		tag = Y01
		NOT = { owns_or_subject_of = 4828 }
		has_faction = oni_golden_gates
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	option = {
		name = flavor_azjakuma.8.a
		ai_chance = { factor = 100 }
		add_stability = -1
		add_devotion = -10
		remove_faction = oni_golden_gates
	}
}

country_event = {
	id = flavor_azjakuma.9
	title =  flavor_azjakuma.9.t
	desc =  flavor_azjakuma.9.d
	picture = COUNTRY_COLLAPSE_eventPicture
	
	trigger = {
		tag = Y01
		NOT = { owns_or_subject_of = 4848 }
		has_faction = oni_cursed_souls
		has_global_flag = khelorvalshi_restored
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	option = {
		name = flavor_azjakuma.9.a
		ai_chance = { factor = 100 }
		add_stability = -1
		add_devotion = -10
		remove_faction = oni_cursed_souls
	}
}

country_event = {
	id = flavor_azjakuma.10
	title =  flavor_azjakuma.10.t
	desc =  flavor_azjakuma.10.d
	picture = COMET_SIGHTED_eventPicture
	
	trigger = {
		tag = Y01
		NOT = { has_faction = oni_highest_fire }
		owns_or_subject_of = 5430
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	option = {
		name = flavor_azjakuma.10.a
		ai_chance = { factor = 100 }
		add_stability = 1
		add_devotion = 10
		add_faction = oni_highest_fire
	}
}

country_event = {
	id = flavor_azjakuma.11
	title =  flavor_azjakuma.11.t
	desc =  flavor_azjakuma.11.d
	picture = COMET_SIGHTED_eventPicture
	
	trigger = {
		tag = Y01
		NOT = { has_faction = oni_silent_mist }
		owns_or_subject_of = 4831
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	option = {
		name = flavor_azjakuma.10.a
		ai_chance = { factor = 100 }
		add_stability = 1
		add_devotion = 10
		add_faction = oni_silent_mist
	}
}

country_event = {
	id = flavor_azjakuma.12
	title =  flavor_azjakuma.12.t
	desc =  flavor_azjakuma.12.d
	picture = COMET_SIGHTED_eventPicture
	
	trigger = {
		tag = Y01
		NOT = { has_faction = oni_bright_claw }
		owns_or_subject_of = 4829
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	option = {
		name = flavor_azjakuma.12.a
		ai_chance = { factor = 100 }
		add_stability = 1
		add_devotion = 10
		add_faction = oni_bright_claw
	}
}

country_event = {
	id = flavor_azjakuma.13
	title =  flavor_azjakuma.13.t
	desc =  flavor_azjakuma.13.d
	picture = COMET_SIGHTED_eventPicture
	
	trigger = {
		tag = Y01
		NOT = { has_faction = oni_golden_gates }
		owns_or_subject_of = 4828
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	option = {
		name = flavor_azjakuma.13.a
		ai_chance = { factor = 100 }
		add_stability = 1
		add_devotion = 10
		add_faction = oni_golden_gates
	}
}

country_event = {
	id = flavor_azjakuma.14
	title =  flavor_azjakuma.14.t
	desc =  flavor_azjakuma.14.d
	picture = COMET_SIGHTED_eventPicture
	
	trigger = {
		tag = Y01
		NOT = { has_faction = oni_cursed_souls }
		owns_or_subject_of = 5430
		has_global_flag = khelorvalshi_restored
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	option = {
		name = flavor_azjakuma.10.a
		ai_chance = { factor = 100 }
		add_stability = 1
		add_devotion = 10
		add_faction = oni_cursed_souls
	}
}